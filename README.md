# Feedly API


## Steps
> Before starting to use this you need to  type your access token on your .env file to get an access token go here
> [https://developer.feedly.com/v3/developer/](https://developer.feedly.com/v3/developer/)
> [https://feedly.com/v3/auth/dev](https://feedly.com/v3/auth/dev)

 1.  copy .env.example to .env 
 2.  npm i
 3. npm run start

Feel free to clone or use this repo