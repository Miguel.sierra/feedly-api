const Axios = require('axios');
const inquirer = require('inquirer');
const dotenv = require('dotenv');
dotenv.config();

setToken =  () => {
    //Pending ask for client id and get the token 
    //newToken = await Axios.post('https://cloudfeedly.com/v3/auth/token')
    return process.env.TOKEN
}

let token = setToken()

let config = {
    headers: {
        Authorization: token
    }
}

let questions = [
    {
        type: 'list',
        name: 'options',
        message: 'Select an option',
        choices: ['User Profile','Get Collections','New Collection', 'Collection Details', 'Update Collection', 'Search feed', 'Add feed to collection', 'Delete Feed', 'Details Feed', 'Exit'],

    }
]


getProfile = async () => {
    let profile
    try {
        profile = await Axios.get('https://cloud.feedly.com/v3/profile',config)
        if (profile.status == 401) {
           setToken()
           getProfile()
        }
        return profile.data
    } catch (error) {
        console.error(error)
    }
}

getCollections = async () => {
    let collections 
    try {
        collections = await Axios.get('https://cloud.feedly.com/v3/collections', config)
        if (collections.status == 401) {
            setToken()
            getCollections()
        }
       return collections.data
    } catch (error) {
        console.error(error)
    }

}

getnewCollection =  async() => {
    let newCollect
    try {
        const collection = await inquirer.prompt([
            {
                type: 'input',
                name: 'collection',
                message: 'Collection name',
            }
        ])
        newCollect = await Axios.post('https://cloud.feedly.com/v3/collections', {
            label: collection.collection,
        }, config)
    
        if (newCollect.status == 401) {
            setToken()
            getnewCollection()
        }
        return newCollect.data
    } catch (error) {
        console.error(error)
        return error
    }
}

deatailsFromCollection = async() => {
    try {
        const id = await inquirer.prompt([
            {
                type: 'input',
                name: 'collection',
                message: 'Collection ID',
            }
        ])
        let collection_id = encodeURIComponent(id.collection)
        let details = await Axios.get(`https://cloud.feedly.com/v3/collections/${collection_id}`, config)
        if (details.status == 401) {
            setToken()
            deatailsFromCollection()
        }
        return details.data
    } catch (error) {
        console.error(error.data)
        return error
    }
}

updateCollection = async() => {
    try {
        const inputs = await inquirer.prompt([
            {
                type: 'input',
                name: 'id',
                message: 'Collection ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            },
            {
                type: 'input',
                name: 'label',
                message: 'Collection name',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need a name'
                    }
                }
            }
        ])
        updateCollection = await Axios.post('https://cloud.feedly.com/v3/collections', {
            label: inputs.label,
            id: inputs.id
        }, config)
    
        if (updateCollection.status == 401) {
            setToken()
            updateCollection()
        }
        return updateCollection.data
    } catch (error) {
        console.error(error)
        return error
    }
}

searchFeed = async () => {
    try {
        const inputs = await inquirer.prompt([
            {
                type: 'input',
                name: 'query',
                message: 'Search query',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need a query'
                    }
                }
            },
        ])
        /*optional paramscount
            count
            Optional number number of results. default value is 20
            locale
            Optional locale hint the search engine to return feeds in that locale (e.g. “pt”, “fr_FR”)*/
        let searchFeed = await Axios.get(`https://cloud.feedly.com/v3/search/feeds/`,{
            params: {
                query: inputs.query
            }
        }, config)
        if (searchFeed.status == 401) {
            setToken()
            searchFeed()
        }
        return searchFeed.data

    } catch (error) {
        console.error(error)
        return error
    }
}

newFeed = async () => {
    try {
    
        const inputs = await inquirer.prompt([
            {
                type: 'input',
                name: 'id',
                message: 'Feed ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            },
            {
                type: 'input',
                name: 'collection_id',
                message: 'collection ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            },
        ])
        let collectionId = encodeURIComponent(inputs.collection_id)
        let addFeed = await Axios.post(`https://cloud.feedly.com/v3/collections/${collectionId}/feeds`, {
            id: inputs.id
        }, config)
        if (addFeed.status == 401) {
            setToken()
            newFeed()
        }
        return addFeed.data

    } catch (error) {
        console.error(error)
        return error
    }
    
}

deleteFeed = async () => {
    try {
    
        const inputs = await inquirer.prompt([
            {
                type: 'input',
                name: 'id',
                message: 'Feed ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            },
            {
                type: 'input',
                name: 'collection_id',
                message: 'collection ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            },
        ])
        let collectionId = encodeURIComponent(inputs.collection_id)
        let feedId = encodeURIComponent(inputs.id)
        let deleteFeed = await Axios.delete(`https://cloud.feedly.com/v3/collections/${collectionId}/feeds/${feedId}`, config)
        if (deleteFeed.status == 401) {
            setToken()
            deleteFeed()
        }
        return deleteFeed.data

    } catch (error) {
        console.error(error)
        return error
    }
}

detailsFeed = async () => {
    try {
        
        const inputs = await inquirer.prompt([
            {
                type: 'input',
                name: 'id',
                message: 'Feed ID',
                validate: function(value){
                    if (value) {
                        return true;
                    }else{
                        return 'Need an ID'
                    }
                }
            }
        ])
        let feed_id = encodeURIComponent(inputs.id)
        let detailsFeed = await Axios.get(`https://cloud.feedly.com/v3/feeds/${feed_id}`, config)
        if (detailsFeed.status == 401) {
            setToken()
            detailsFeed()
        }
        return detailsFeed.data

    } catch (error) {
        console.error(error)
        return error
    }
}

const menu = () => {
    console.log('-----------------------------------------------------------------------------')
    inquirer
      .prompt(questions)
      .then(answers => {
        switch (answers.options) {
            case 'User Profile':
                getProfile().then(response => {
                    console.log(response)
                    menu()
                })
                break;
    
            case 'Get Collections':
                getCollections().then(response => {
                    console.log(response)
                    menu()
                })
                break;
    
            case 'New Collection':
                getnewCollection().then(response => {
                    console.log(response)
                    menu()
                })
                break;
            
            case 'Update Collection':
                updateCollection().then(response=>{
                    console.log(response)
                    menu()
                })
                break;
        
            case 'Collection Details':
                deatailsFromCollection().then(response => {
                    console.log(response)
                    menu()
                })
                break;
            
            case 'Search feed':
                searchFeed().then(response => {
                    console.log(response)
                    menu()
                })
                break;

            
            case 'Add feed to collection':
                newFeed().then(response => {
                    console.log(response)
                    menu()
                })
                break;

            case 'Details Feed':
                detailsFeed().then(response => {
                    console.log(response)
                    menu()
                })
                break;

            case 'Delete Feed':
                deleteFeed().then(response => {
                    console.log(response)
                    menu()
                })
                break;

            case 'Exit':
                process.exit();
                break;

            default:
                break;
        }
      });


}

menu()